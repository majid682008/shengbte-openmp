export FFLAGS=-traceback -debug -O3 -ipo -xHost -static_intel -qopenmp
export LDFLAGS=-L/home/majid/Apps/shengbte/spglib-1.9.4/lib -lsymspg -qopenmp
export FC=ifort
MKL=$(MKLROOT)/lib/intel64/libmkl_lapack95_lp64.a -Wl,--start-group	\
$(MKLROOT)/lib/intel64/libmkl_intel_lp64.a				\
 $(MKLROOT)/lib/intel64/libmkl_sequential.a				\
 $(MKLROOT)/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm
export LAPACK=$(MKL)
export LIBS=$(LAPACK)
