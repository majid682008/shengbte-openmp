# ShengBTE
ShengBTE is a software package for solving the Boltzmann Transport Equation for phonons. Its main purpose is to compute the lattice contribution to the thermal conductivity of bulk crystalline solids.

# OpenMP
This is the OpenMP version of ShengBTE. This version is very efficient in using memory. To find the original version follow this [link](https://www.shengbte.org/).

